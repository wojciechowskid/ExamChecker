package pl.com.tt.examiner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import lombok.extern.slf4j.Slf4j;
import pl.com.tt.config.ExamConfig;
import pl.com.tt.config.RunnerConfig;
import pl.com.tt.model.Criteria;
import pl.com.tt.model.Project;

@Slf4j
public class ExamCheckerImpl implements ExamChecker {

    private ExamConfig examConfig;
    private RunnerConfig runnerConfig;
    private StringBuilder builder;

    public ExamCheckerImpl(ExamConfig examConfig, RunnerConfig runnerConfig) {
        this.examConfig = examConfig;
        this.runnerConfig = runnerConfig;
    }

    @Override
    public List<Project> examine(List<Project> projects) {
        projects = fillObjectives(projects);
        for (Project project : projects) {
        	log.debug("Processing project made by : " + project.getStudent().getName()+ " " +project.getStudent().getSurname() );
            if (project.isCompiled()) {
                for (Criteria criteria : examConfig.getCriterias()) {
                    project = processCriteria(project, criteria);
                }
            }
        }
        return projects;
    }
    private Project processCriteria(Project project, Criteria criteria) {
        try {
            String command = runnerConfig.getCompileCommand() + " " + project.getProjectRoot() + runnerConfig.getApplicationShortPath();
            log.debug("Attempting to launch program");
            Process process = Runtime.getRuntime().exec(command);

            OutputStream stdin = process.getOutputStream();
            InputStream stdout = process.getInputStream();

            log.debug("Writting input criterias");
            writeInputCriterias(criteria, stdin);

            log.debug("Reading Output");
            Boolean status = processOutputCriterias(criteria, stdout);
            project.getObjectives().put(criteria.getName(), status);
            
            stdin.close();
            if (process.isAlive()) {
            	log.debug("Program not closed properly. Shutting down.");
                process.destroy();
            }
        } catch (Exception e) {
            project.getObjectives().put(criteria.getName(), false);
            log.debug("Project could not be run for criteria: "+criteria.getName());
        }
        return project;
    }

    private boolean processOutputCriterias(Criteria criteria, InputStream stdout) throws IOException {
        String output = getOutput(stdout);
        log.debug("CRITERIA: "+ criteria.getName());
        for (String outputCriteria : criteria.getOutputCriteria()) {
            if (!output.contains(outputCriteria)) {
                return false;
            }
        }
        for(String excludedOutputCriteria : criteria.getExcludedOutputCriteria()){
            if (output.contains(excludedOutputCriteria)) {
                return false;
            }
        }
        return true;
    }

    private void writeInputCriterias(Criteria criteria, OutputStream stdin) throws IOException {
        for (String inputCrit : criteria.getInputCriteria()) {
            String line = inputCrit + "\n";
            stdin.write(line.getBytes());
            stdin.flush();
        }
    }

    private String getOutput(InputStream stdout) throws IOException {

        ExecutorService service = Executors.newSingleThreadExecutor();

        try {
            final Future<Object> f = service.submit(() -> {
                String line;
                builder = new StringBuilder();
                BufferedReader brCleanUp = new BufferedReader(new InputStreamReader(stdout));
                while ((line = brCleanUp.readLine()) != null) {
                    builder.append(line);
                }
                brCleanUp.close();
                return 1;
            });
            f.get(10, TimeUnit.SECONDS);
        } catch (final Exception e) {
        	log.error("Error during fetch of output");
        	
        } finally {
            service.shutdown();
        }
        String output = builder.toString();
        log.debug("String lenght: "+output.length());
        return builder.toString();
        
    }

    private List<Project> fillObjectives(List<Project> projects) {
        for (Project project : projects) {
            examConfig.getCriterias().stream()
                    .forEach(c -> project.getObjectives().put(c.getName(), false));
        }
        return projects;
    }


}
