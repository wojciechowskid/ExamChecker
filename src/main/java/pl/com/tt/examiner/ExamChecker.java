package pl.com.tt.examiner;

import java.util.List;

import pl.com.tt.model.Project;

public interface ExamChecker {
	
	List<Project> examine(List<Project> toExamine);
	
}
