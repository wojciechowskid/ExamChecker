package pl.com.tt.parser;

import java.util.Optional;

import pl.com.tt.config.ApplicationConfig;
import pl.com.tt.config.CompilerConfig;
import pl.com.tt.config.ExamConfig;
import pl.com.tt.config.RunnerConfig;

public interface XMLParser {
	
	Optional<CompilerConfig> getCompilerConfig();
	Optional<ApplicationConfig> getApplicationConfig();
	Optional<ExamConfig> getExamConfig();
	Optional<RunnerConfig> getRunnerConfig();
}
