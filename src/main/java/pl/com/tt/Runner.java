package pl.com.tt;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import lombok.extern.slf4j.Slf4j;
import pl.com.tt.config.ApplicationConfig;
import pl.com.tt.config.CompilerConfig;
import pl.com.tt.config.ExamConfig;
import pl.com.tt.config.RunnerConfig;
import pl.com.tt.examiner.ExamChecker;
import pl.com.tt.examiner.ExamCheckerImpl;
import pl.com.tt.model.Criteria;
import pl.com.tt.model.Project;
import pl.com.tt.parser.XLSXWriter;
import pl.com.tt.parser.XMLParser;
import pl.com.tt.parser.XSLXWriterImpl;
import pl.com.tt.parser.XpathParser;
import pl.com.tt.utility.Compiler;
import pl.com.tt.utility.CompilerImpl;

@Slf4j
public class Runner {

	public static void main(String[] args){

		try {
			XMLParser parser = new XpathParser();
			log.debug("XMLParser: " + parser.getClass() + ".Has been initialized");

			Optional<CompilerConfig> compilerConfig = parser.getCompilerConfig();
			Optional<ApplicationConfig> appConfig = parser.getApplicationConfig();
			Optional<ExamConfig> examConfig = parser.getExamConfig();
			Optional<RunnerConfig> runnerConfig = parser.getRunnerConfig();

			if (compilerConfig.isPresent() && appConfig.isPresent() && examConfig.isPresent()
					&& runnerConfig.isPresent()) {
				log.debug("Configs loaded properly");

				Compiler compiler = new CompilerImpl(compilerConfig.get());
				List<Project> projects = compiler.compileUnderRootFolder(appConfig.get().getApplicationRootFolder());
				ExamChecker examChecker = new ExamCheckerImpl(examConfig.get(), runnerConfig.get());

				projects = examChecker.examine(projects);
				List<String> headers = examConfig.get().getCriterias().stream().map(Criteria::getName)
						.collect(Collectors.toList());
				
				XLSXWriter writer = new XSLXWriterImpl(projects, headers);
				writer.writeProjectsToXLSXFile();
				System.out.println("END");
			}

		} catch (FileNotFoundException e) {
			log.error("Could not find config file");
		} catch (ParserConfigurationException | SAXException | IOException e) {
			log.error("Problem with CONFIG xml or excel file");
		}
	}

}
