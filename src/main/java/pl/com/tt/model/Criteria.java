package pl.com.tt.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Criteria {

	private String name;
	private List<String> inputCriteria;
	private List<String> outputCriteria;
	private List<String> excludedOutputCriteria;

}
