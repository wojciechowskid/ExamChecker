package pl.com.tt.config;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CompilerConfigIImpl implements CompilerConfig {

	private String compilerName;
	private String command;
	private String buildFileName;

	@Override
	public String getCompileCommand() {
		return command;
	}

	@Override
	public String getBuildFileName() {
		return buildFileName;
	}

	@Override
	public String getCompilerName() {
		return compilerName;
	}

}
