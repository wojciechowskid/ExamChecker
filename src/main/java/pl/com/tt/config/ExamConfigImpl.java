package pl.com.tt.config;

import java.util.List;

import pl.com.tt.model.Criteria;

public class ExamConfigImpl implements ExamConfig{
	
	private final List<Criteria> criterias;

	public ExamConfigImpl(List<Criteria> criterias) {
		this.criterias = criterias;
	}

	@Override
	public List<Criteria> getCriterias() {
		return criterias;
	}	

}
