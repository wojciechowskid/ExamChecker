package pl.com.tt.config;

import java.io.File;

public class ApplicationConfig {

	private final File applicationRootFolder;

	public ApplicationConfig(File applicationRootFolder) {
		this.applicationRootFolder = applicationRootFolder;
	}

	public File getApplicationRootFolder() {
		return applicationRootFolder;
	}

}
