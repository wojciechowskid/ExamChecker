package pl.com.tt.config;

public interface CompilerConfig {

	String getCompileCommand();
	String getBuildFileName();
	String getCompilerName();
	
}
