package pl.com.tt.utility;

import java.io.File;
import java.util.List;

import pl.com.tt.model.Project;

public interface Compiler {

	List<File> compileAllUnderRootFolder(File rootFolder);
	List<File> compileProjects(List<File> toCompile) ;

	List<Project> compileUnderRootFolder(File rootFolder);
	List<Project> compile(List<File> toCompile) ;

}
