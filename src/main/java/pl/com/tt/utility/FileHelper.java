package pl.com.tt.utility;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class FileHelper {
	
	public static List<File> getAllSubfolders(File rootFolder){
		return Arrays.asList(rootFolder.listFiles(File::isDirectory));
	}

}
